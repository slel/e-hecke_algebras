# -*- coding: utf-8 -*-
r"""
e-Hecke algebras
================


AUTHORS:

- Hankyung Ko
- Samuel Lelièvre
"""
# ****************************************************************************
#       Copyright (C) 2019 Hankyung Ko <hk2kn@virginia.edu>
#
#  Distributed under the terms of the GNU General Public License (GPL)
#
#  The full text of the GPL is available at:
#
#                  https://www.gnu.org/licenses/
# ****************************************************************************

def tentative_eigenvalues(n, w, candidates=None, p=None, qq=None):
    r"""
    Return eigenvalue candidates for this n and w.

    INPUT:
    
    - ``n`` (integer) -- the rank ('e')

    - ``w`` -- the element of the Iwahori-Hecke algebra to use
      which is the first standard generator of n-Hecke algebras 
    
    - ``candidates`` (optional, default: None) -- either ``None```
      or a list of candidate eigenvalues to test

    - ``p`` (optional, default: None) -- either ``None``` or a prime
      that will be used to create a prime field

    - ``qq`` (optional, default: None) -- either ``None`` or an
      integer to be used as a finite field element to special-case
      the determinant for testing the candidate eigenvalues

    This consctructs the `A_m` Iwahori-Hecke algebra
    where `m = 2 n - 1`, transforms `w` to a matrix `m`,
    substitutes a finite field element for `q` in `m`,
    and checks which `lambda` are eigenvalues
    of this modified matrix, for `lambda` in either
    the candidate list provided as an argument, or among
    all `q^i` and `-q^i` for `i` from `-n^2` to `n^2`.

    If no list of candidates is provided, or if all
    eigenvalues of `w` were in the candidate list provided,
    then all eigenvalues of `w` will be in the returned list,
    but the list may contain elements which are not
    eigenvalues of `w`.

    If ``p`` is ``None``, then `p = 5` will be used.

    If ``qq`` is ``None``, then a random element in the prime
    field `F_p` will be used, avoiding zero, one and minus one.

    EXAMPLES::

        sage: R = LaurentPolynomialRing(ZZ)
        sage: q = R.gen()

        sage: H = IwahoriHeckeAlgebra('A3', q, -q**-1)
        sage: T = H.T()
        sage: w = T[2, 3, 1, 2]
        sage: tentative_eigenvalues(2, w, p=31, qq=10)
        [q^-4, q^-2, -q^-2, -1, q^2, -q^2, q^4]

        sage: H = IwahoriHeckeAlgebra('A3', q, -q**-1)
        sage: T = H.T()
        sage: w = T[2, 3, 1, 2]
        sage: tentative_eigenvalues(2, w, p=31, qq=10)
        [q^-4, q^-2, -q^-2, -1, q^2, -q^2, q^4]


        sage: H = IwahoriHeckeAlgebra('A5', q, -q**-1)
        sage: T = H.T()
        sage: w = T[3, 2, 1, 4, 3, 2, 5, 4, 3]
        sage: tentative_eigenvalues(3, w, p=31, qq=10)
        [q^-9, -q^-9, q^-6, -q^-6, q^-5, -q^-5, q^-3, -q^-3,
         q^-2, -q^-2, q^-1, 1, -1, -q, q^2, -q^2, q^3, -q^3,
         q^5, -q^5, q^6, -q^6, q^9, -q^9]
    """
    R = LaurentPolynomialRing(ZZ)
    q = R.gen()
    H = IwahoriHeckeAlgebra('A{}'.format(2*n - 1), q, -q**-1)
    m = w.to_matrix()
    M = m.matrix_space(sparse=True)
    m = M(m)
    if p is None:
        p = 5
    if qq is None:
        qq = GF(p)(randint(2, p - 2))
    else:
        qq = GF(p)(qq)
    res = []
    if candidates is not None:
        for la in candidates:
            d = (m - la).subs(q=qq).det()
            if d.is_zero():
                res.append(la)
    else:
        nn = n^2
        for i in srange(-nn, nn + 1):
            la = q**i
            d = (m - la).subs(q=qq).det()
            if d.is_zero():
                res.append(la)
            d = (m + la).subs(q=qq).det()
            if d.is_zero():
                res.append(-la)
    return res
